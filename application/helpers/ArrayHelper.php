<?php

namespace app\helpers;


class ArrayHelper extends \yii\helpers\ArrayHelper
{
    public static function search($array, $search, $key)
    {
        foreach ($array as $k => $val) {
            if ($val[$key] == $search) {
                return $array[$k];
            }
        }
        return null;
    }
}
