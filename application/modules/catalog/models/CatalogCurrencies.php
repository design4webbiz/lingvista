<?php

namespace app\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "{{%catalog_currencies}}".
 *
 * @property integer $id
 * @property string $iso_code
 * @property string $symbol
 * @property string $unicode
 * @property string $position
 * @property string $comments
 * @property string $timestamp
 * @property string $equivalent_currency
 */
class CatalogCurrencies extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_currencies}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position'], 'string'],
            [['timestamp'], 'safe'],
            [['iso_code', 'symbol'], 'string', 'max' => 3],
            [['unicode'], 'string', 'max' => 8],
            [['comments'], 'string', 'max' => 255],
            [['equivalent_currency'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => Yii::t('catalog', 'ID'),
            'iso_code'            => Yii::t('catalog', 'Iso Code'),
            'symbol'              => Yii::t('catalog', 'Symbol'),
            'unicode'             => Yii::t('catalog', 'Unicode'),
            'position'            => Yii::t('catalog', 'Position'),
            'comments'            => Yii::t('catalog', 'Comments'),
            'timestamp'           => Yii::t('catalog', 'Timestamp'),
            'equivalent_currency' => Yii::t('catalog', 'Equivalent Currency'),
        ];
    }
}
