<?php

namespace app\modules\catalog\models\forms;

use app\modules\catalog\models\CatalogCategory;
use app\modules\catalog\models\CatalogCategoryLang;
use app\modules\catalog\models\CatalogCountries;
use app\modules\catalog\models\CatalogCountriesLang;
use app\modules\catalog\models\CatalogCurrencies;
use app\modules\catalog\models\CatalogItems;
use app\modules\catalog\models\CatalogItemsLang;
use app\modules\catalog\models\CatalogPrices;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class Filter extends Model
{
    /** @var string */
    public $title;

    /** @var integer */
    public $specialization;

    /** @var integer */
    public $country;

    /** @var float */
    public $price_from;

    /** @var float */
    public $price_to;

    /** @var integer */
    public $currency;

    /** @var integer */
    public $free;

    /** @var integer */
    public $hot;

    /** @var integer */
    public $video;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['specialization', 'country', 'currency'], 'string', 'max' => 255],
            [['free', 'hot', 'video'], 'integer'],
            [['price_from', 'price_to'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'free' => \Yii::t('catalog', 'Free job openings'),
            'hot' => \Yii::t('catalog', 'Sticking vacancies'),
            'video' => \Yii::t('catalog', 'With video review'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CatalogItems::find()
            ->joinWith(['translations'])
            ->where([CatalogItemsLang::tableName().'.language' => \Yii::$app->language]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 40,
                'forcePageParam'  => false,
                'pageSizeParam'   => false,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $currencies = CatalogCurrencies::find()->asArray()->all();
        $price_filter = ['or'];
        foreach ($currencies as $currency) {
            if($currency['id'] == $this->currency) {
                $price_filter[] = [
                    'and',
                    [CatalogItems::tableName().'.currency_id' => $this->currency],
                    ['>=', CatalogItems::tableName().'.price_from', $this->price_from]
                ];
            } else {
                $coreCurrency = \app\helpers\ArrayHelper::search($currencies, $this->currency, 'id');
                $equivalents = \Symfony\Component\Yaml\Yaml::parse($coreCurrency['equivalent_currency']);
                $price_from = round($this->price_from * $equivalents[$currency['iso_code']]);

                $price_filter[] = [
                    'and',
                    [CatalogItems::tableName().'.currency_id' => $currency['id']],
                    ['>=', CatalogItems::tableName().'.price_from', $price_from]
                ];
            }
        }
        $query
            ->andFilterWhere([
                'or',
                [CatalogItems::tableName().'.identifier' => $this->title],
                ['like', CatalogItemsLang::tableName().'.title', $this->title],
            ])
            ->andFilterWhere([CatalogItems::tableName().'.specialization_id' => $this->specialization])
            ->andFilterWhere([CatalogItems::tableName().'.catalog_country_id' => $this->country])
            ->andFilterWhere($price_filter)
            ->andFilterWhere([CatalogItems::tableName().'.is_hot' => $this->hot])
            ->andFilterWhere([CatalogItems::tableName().'.is_free' => $this->free])
            ->andFilterWhere([CatalogItems::tableName().'.is_video' => $this->video]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        $currencies = CatalogCurrencies::find()->asArray()->all();
        $return = ArrayHelper::map($currencies, 'id', 'iso_code');
        return $return;
    }

    /**
     * @return array
     */
    public function getSpecializations()
    {
        $specializations = CatalogCategory::find()
            ->where(['status' => CatalogCategory::STATUS_PUBLISHED])
            ->all();
        $return = ArrayHelper::map($specializations, 'id', 'title');
        return $return;
    }

    /**
     * @return array
     */
    public function getCountries()
    {
        $countries = CatalogCountries::find()
            ->where(['status' => CatalogCountries::STATUS_PUBLISHED])
            ->all();

        $return = ArrayHelper::map($countries, 'id', 'title');
        return $return;
    }
}
