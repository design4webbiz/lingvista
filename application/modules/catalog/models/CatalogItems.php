<?php

namespace app\modules\catalog\models;

use app\components\ActiveQuery;
use app\components\ActiveRecord;
use app\modules\gallery\behaviors\AttachImages;
use dosamigos\translateable\TranslateableBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "catalog_items".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property integer $specialization_id
 * @property integer $gender
 * @property integer $age
 * @property integer $catalog_country_id
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $is_hot
 * @property integer $is_free
 * @property integer $is_video
 * @property integer $widgetkit
 * @property string $widgetkit_title
 * @property string $salary
 * @property integer $experience
 * @property integer $identifier
 * @property integer $show_home
 * @property integer $price_from
 * @property integer $price_to
 * @property integer $currency_id
 * @property string $duties
 *
 * @mixin AttachImages
 */

class CatalogItems extends ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'specialization_id',
                    'identifier',
                    'catalog_country_id',
                    'show_home',
                    'status'
                ],
                'required'
            ],
            [
                [
                    'specialization_id',
                    'gender',
                    'identifier',
                    'catalog_country_id',
                    'status',
                    'show_home',
                    'is_hot',
                    'is_free',
                    'is_video',
                    'widgetkit',
                    'review_block_id',
                    'currency_id',
                    'price_to',
                    'price_from',
                ],
                'integer'
            ],
            [
                [
                    'conditions_of_employment',
                    'requirements',
                    'work_schedule',
                    'age',
                    'duties',
                    'salary',
                    'title',
                    'slug',
                    'experience',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                    'widgetkit_title'
                ],
                'string'
            ],
            ['identifier', 'unique'],

            [['slug'], 'string', 'max' => 150],

            [
                ['slug'],
                'unique',
                'targetClass' => CatalogItemsLang::className(),
                'targetAttribute' => 'slug',
                'filter' => function ($query) {
                    /**
                     * @var $query ActiveQuery
                     */
                    if(!$this->isNewRecord)
                        $query->andWhere('catalog_items_id <> :a_id', ['a_id' => $this->id]);

                    return $query;
                }
            ],

            ['published_at', 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            ['published_at', 'default', 'value' => time()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                TimestampBehavior::className(),
                [
                    'class'     => SluggableBehavior::className(),
                    'attribute' => 'title',
                    'immutable' => true,
                ],
                'images' => [
                    'class' => AttachImages::className(),
                    'mode'  => 'single'
                ],
                'trans' => [
                    'class'                 => TranslateableBehavior::className(),
                    'translationAttributes' => [
                        'title', 'slug', 'age', 'duties',
                        'conditions_of_employment', 'requirements', 'work_schedule',
                        'meta_title', 'salary', 'meta_keywords', 'meta_description', 'experience', 'widgetkit_title'
                    ],
                ],
            ];
    }

    /**
     * @param null $key
     *
     * @return array|mixed
     */
    public function getGender($key = null) {
        $array = [
            1 => Yii::t('catalog', 'Men'),
            2 => Yii::t('catalog', 'Women'),
            3 => Yii::t('catalog', 'Men and women')
        ];

        if($key) {
            return $array[$key];
        }

        return $array;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CatalogItemsLang::className(), ['catalog_items_id' => 'id']);
    }

    public function getPrices()
    {
        return $this->hasMany(CatalogPrices::className(), ['catalog_items_id' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(CatalogCategory::className(), ['id' => 'specialization_id']);
    }

    public function getCountry()
    {
        return $this->hasOne(CatalogCountries::className(), ['id' => 'catalog_country_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFrontendViewLink()
    {
        return ['/catalog/item/view', 'catslug' => $this->category->slug, 'slug' => $this->slug];
    }

    /**
     * @inheritdoc
     */
    public static function frontendViewLink($model)
    {
        return ['/catalog/item/view', 'catslug' => $model->category->slug, 'slug' => $model->slug];
    }

    /**
     * @inheritdoc
     */
    public function getBackendViewLink()
    {
        return ['/admin/catalog/item/update', 'id' => $this->id, 'language' => Yii::$app->language];
    }

    /**
     * @inheritdoc
     */
    public static function backendViewLink($model)
    {
        return ['/admin/catalog/item/update', 'id' => $model['id'], 'language' => Yii::$app->language];
    }

    public function afterDelete()
    {
        parent::afterDelete();

        // Видалення перекладу
        foreach ($this->getTranslations()->all() as $translations) {
            $translations->delete();
        }

        foreach ($this->getPrices()->all() as $prices) {
            $prices->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('catalog', 'ID'),
            'specialization_id'  => Yii::t('catalog', 'Specialization'),
            'gender'             => Yii::t('catalog', 'Gender'),
            'age'                => Yii::t('catalog', 'Age'),
            'catalog_country_id' => Yii::t('catalog', 'Country'),
            'published_at'       => Yii::t('catalog', 'Published At'),
            'created_at'         => Yii::t('catalog', 'Created At'),
            'updated_at'         => Yii::t('catalog', 'Updated At'),
            'status'             => Yii::t('catalog', 'Status'),
            'is_hot'             => Yii::t('catalog', 'Is Hot'),
            'is_free'            => Yii::t('catalog', 'Is Free'),
            'is_video'           => Yii::t('catalog', 'Is Video'),
            'widgetkit_title'    => Yii::t('catalog', 'Certeficates Title'),
            'widgetkit'          => Yii::t('catalog', 'Certeficates'),
            'experience'         => Yii::t('catalog', 'Experience'),
        ];
    }
}
