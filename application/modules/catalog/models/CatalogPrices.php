<?php

namespace app\modules\catalog\models;

use Yii;
use yii\validators\UniqueValidator;

/**
 * This is the model class for table "{{%catalog_prices}}".
 *
 * @property integer $id
 * @property integer $catalog_items_id
 * @property integer $catalog_currency_id
 * @property integer $price_from
 * @property integer $price_to
 */
class CatalogPrices extends \yii\db\ActiveRecord
{

    public $convert = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_prices}}';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['price_from', 'price_to'], 'required'],
            [['catalog_items_id', 'catalog_currency_id', 'price_from', 'price_to'], 'integer'],
            [
                [
                    'catalog_items_id',
                    'catalog_currency_id',
                ],
                'unique',
                'targetAttribute' => [
                    'catalog_items_id',
                    'catalog_currency_id',
                ],
                'comboNotUnique'  => Yii::t(
                    'yii',
                    'The combination {values} of {attributes} has already been taken.',
                    ['values' => '[валюта - товар]', 'attributes' => 'цены']
                ),
            ],

            ['convert', 'boolean', 'message' => $this->convert],
        ];
    }

    public function getCurrency()
    {
        return $this->hasOne(CatalogCurrencies::className(), ['id' => 'catalog_currency_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => Yii::t('catalog', 'ID'),
            'catalog_items_id'    => Yii::t('catalog', 'Catalog Items ID'),
            'catalog_currency_id' => Yii::t('catalog', 'Catalog Currency ID'),
            'price_from'          => Yii::t('catalog', 'Price From'),
            'price_to'            => Yii::t('catalog', 'Price To'),
            'convert'             => Yii::t('catalog', 'Convert to other currencies'),
        ];
    }

    public static function editField($id, $name, $value)
    {
        $setting = self::findOne($id);
        $setting->$name = $value;
        $setting->save();
    }
}
