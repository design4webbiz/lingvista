<?php

namespace app\modules\catalog\models;

use app\components\ActiveRecord;
use dosamigos\translateable\TranslateableBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "catalog_countries".
 *
 * @property integer $id
 * @property string $title
 * @property string $flag
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class CatalogCountries extends ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flag', 'title', 'status'], 'required'],
            [['created_at', 'updated_at', 'status'], 'integer'],
            [['flag'], 'string', 'max' => 255],
            [['published_at'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            ['published_at', 'default', 'value' => time()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                TimestampBehavior::className(),
                'trans' => [
                    'class' => TranslateableBehavior::className(),
                    'translationAttributes' => ['title']
                ]
            ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CatalogCountriesLang::className(), ['catalog_country_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('catalog', 'ID'),
            'title' => Yii::t('catalog', 'Title'),
            'flag' => Yii::t('catalog', 'Flag'),
            'published_at' => Yii::t('catalog', 'Published At'),
            'created_at' => Yii::t('catalog', 'Created At'),
            'updated_at' => Yii::t('catalog', 'Updated At'),
            'status' => Yii::t('catalog', 'Status'),
        ];
    }
}
