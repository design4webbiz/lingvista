<?php


namespace app\modules\catalog\models\search;

use app\modules\catalog\models\CatalogPrices;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Price;

class CatalogPricesSearch extends CatalogPrices
{
    public function rules()
    {
        return [
            [['catalog_items_id', 'catalog_currency_id', 'price_from', 'price_to'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CatalogPrices::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'                  => $this->id,
            'catalog_items_id'    => $this->catalog_items_id,
            'catalog_currency_id' => $this->catalog_currency_id,
            'price_to'            => $this->price_to,
            'price_from'          => $this->price_from,
        ]);

        return $dataProvider;
    }
}

