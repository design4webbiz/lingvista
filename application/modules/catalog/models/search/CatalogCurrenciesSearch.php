<?php

namespace app\modules\catalog\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\catalog\models\CatalogCurrencies;

/**
 * CatalogCurrenciesSearch represents the model behind the search form about `app\modules\catalog\models\CatalogCurrencies`.
 */
class CatalogCurrenciesSearch extends CatalogCurrencies
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['iso_code', 'symbol', 'flag', 'unicode', 'position', 'comments', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogCurrencies::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'iso_code', $this->iso_code])
            ->andFilterWhere(['like', 'symbol', $this->symbol])
            ->andFilterWhere(['like', 'flag', $this->flag])
            ->andFilterWhere(['like', 'unicode', $this->unicode])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'comments', $this->comments]);

        return $dataProvider;
    }
}
