<?php

namespace app\modules\catalog\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\catalog\models\CatalogItems;

/**
 * CatalogItemsSearch represents the model behind the search form about `app\modules\catalog\models\CatalogItems`.
 */
class CatalogItemsSearch extends CatalogItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'specialization_id', 'identifier', 'gender', 'age', 'catalog_country_id', 'published_at', 'created_at', 'updated_at', 'status', 'is_hot', 'is_free', 'is_video', 'widgetkit'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogItems::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'specialization_id' => $this->specialization_id,
            'identifier' => $this->identifier,
            'gender' => $this->gender,
            'age' => $this->age,
            'catalog_country_id' => $this->catalog_country_id,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
            'is_hot' => $this->is_hot,
            'is_free' => $this->is_free,
            'is_video' => $this->is_video,
            'widgetkit' => $this->widgetkit,
        ]);

        return $dataProvider;
    }
}
