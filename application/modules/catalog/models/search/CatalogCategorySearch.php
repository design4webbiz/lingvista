<?php

namespace app\modules\catalog\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\catalog\models\CatalogCategory;

/**
 * CatalogCategorySearch represents the model behind the search form about `app\modules\catalog\models\CatalogCategory`.
 */
class CatalogCategorySearch extends CatalogCategory
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if(isset($behaviors['slug'])) unset($behaviors['slug']);
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['title', 'slug', 'published_at'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogCategory::find()
            ->joinWith(['translations'])
            ->where(['{{%catalog_category_lang}}.language' => Yii::$app->language]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'title' => [
                    'asc' => ['{{%catalog_category_lang}}.title' => SORT_ASC],
                    'desc' => ['{{%catalog_category_lang}}.title' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'slug' => [
                    'asc' => ['{{%catalog_category_lang}}.slug' => SORT_ASC],
                    'desc' => ['{{%catalog_category_lang}}.slug' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'published_at',
                'status'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (
            !is_null($this->published_at) &&
            strpos($this->published_at, ' - ') !== false
        ) {
            list($start_date, $end_date) = explode(' - ', $this->published_at);
            $query->andFilterWhere(['between', 'published_at', strtotime($start_date), strtotime($end_date)+86399]);
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
