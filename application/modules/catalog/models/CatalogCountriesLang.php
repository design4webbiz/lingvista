<?php

namespace app\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "catalog_countries_lang".
 *
 * @property integer $id
 * @property integer $catalog_country_id
 * @property string $title
 * @property string $language
 */
class CatalogCountriesLang extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_countries_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_country_id'], 'integer'],
            [['title', 'language'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('catalog', 'ID'),
            'catalog_country_id' => Yii::t('catalog', 'Catalog Country ID'),
            'title' => Yii::t('catalog', 'Title'),
            'language' => Yii::t('catalog', 'Language'),
        ];
    }
}
