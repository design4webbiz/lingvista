<?php

namespace app\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "catalog_items_lang".
 *
 * @property integer $id
 * @property integer $catalog_items_id
 * @property string  $title
 * @property string  $slug
 * @property string  $duties
 * @property string  $conditions_of_employment
 * @property string  $requirements
 * @property string  $work_schedule
 * @property string  $meta_title
 * @property string  $meta_keywords
 * @property string  $meta_description
 * @property string  $widgetkit_title
 * @property string  $language
 */
class CatalogItemsLang extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_items_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_items_id'], 'integer'],
            [['title', 'slug', 'language'], 'required'],
            [['title', 'widgetkit_title', 'slug', 'age', 'duties', 'salary', 'conditions_of_employment', 'requirements', 'work_schedule', 'meta_title', 'meta_keywords', 'meta_description', 'experience'], 'string'],
            [['language'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                       => Yii::t('catalog', 'ID'),
            'title'                    => Yii::t('catalog', 'Title'),
            'slug'                     => Yii::t('catalog', 'Slug'),
            'duties'                   => Yii::t('catalog', 'Duties'),
            'conditions_of_employment' => Yii::t('catalog', 'Conditions Of Employment'),
            'requirements'             => Yii::t('catalog', 'Requirements'),
            'work_schedule'            => Yii::t('catalog', 'Work Schedule'),
            'meta_title'               => Yii::t('catalog', 'Meta Title'),
            'meta_keywords'            => Yii::t('catalog', 'Meta Keywords'),
            'meta_description'         => Yii::t('catalog', 'Meta Description'),
            'language'                 => Yii::t('catalog', 'Language'),
        ];
    }
}
