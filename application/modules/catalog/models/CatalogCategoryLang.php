<?php

namespace app\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "catalog_category_lang".
 *
 * @property integer $id
 * @property integer $catalog_category_id
 * @property string $title
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $language
 */
class CatalogCategoryLang extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_category_lang}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_category_id'], 'integer'],
            [['title', 'slug', 'language'], 'required'],
            [['title', 'title_h1', 'content', 'slug', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('catalog', 'ID'),
            'catalog_category_id' => Yii::t('catalog', 'Catalog Category ID'),
            'title' => Yii::t('catalog', 'Title'),
            'slug' => Yii::t('catalog', 'Slug'),
            'meta_title' => Yii::t('catalog', 'Meta Title'),
            'meta_keywords' => Yii::t('catalog', 'Meta Keywords'),
            'meta_description' => Yii::t('catalog', 'Meta Description'),
            'language' => Yii::t('catalog', 'Language'),
        ];
    }
}
