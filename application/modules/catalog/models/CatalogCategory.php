<?php

namespace app\modules\catalog\models;

use app\components\ActiveQuery;
use app\components\ActiveRecord;
use dosamigos\translateable\TranslateableBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%catalog_category}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $title_h1
 * @property string $content
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $background
 * @property integer $status
 * @property integer $show_home
 */
class CatalogCategory extends ActiveRecord
{
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_category}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                TimestampBehavior::className(),
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'title',
                    'immutable' => true
                ],
                'trans' => [
                    'class' => TranslateableBehavior::className(),
                    'translationAttributes' => [
                        'title', 'title_h1', 'content', 'slug', 'meta_title', 'meta_keywords', 'meta_description'
                    ]
                ]
            ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CatalogCategoryLang::className(), ['catalog_category_id' => 'id']);
    }

    public function getItems()
    {
        return $this->hasMany(CatalogItems::className(), ['specialization_id' => 'id'])
            ->joinWith(['translations'])
            ->andWhere([
                CatalogItems::tableName() . '.status'       => CatalogItems::STATUS_PUBLISHED,
                CatalogItemsLang::tableName() . '.language' => Yii::$app->language,
            ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status', 'show_home'], 'required'],
            [['created_at', 'updated_at', 'status', 'show_home'], 'integer'],
            [['title', 'slug', 'title_h1', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'background'], 'string'],
            [['slug'], 'string', 'max' => 150],
            [
                ['slug'],
                'unique',
                'targetClass' => CatalogCategoryLang::className(),
                'targetAttribute' => 'slug',
                'filter' => function ($query) {
                    /**
                     * @var $query ActiveQuery
                     */
                    if(!$this->isNewRecord)
                        $query->andWhere('catalog_category_id <> :a_id', ['a_id' => $this->id]);

                    return $query;
                }
            ],
            [['published_at'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            ['published_at', 'default', 'value' => time()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('catalog', 'ID'),
            'title' => Yii::t('catalog', 'Title'),
            'slug' => Yii::t('catalog', 'Slug'),
            'meta_title' => Yii::t('catalog', 'Meta Title'),
            'meta_keywords' => Yii::t('catalog', 'Meta Keywords'),
            'meta_description' => Yii::t('catalog', 'Meta Description'),
            'published_at' => Yii::t('catalog', 'Published At'),
            'created_at' => Yii::t('catalog', 'Created At'),
            'updated_at' => Yii::t('catalog', 'Updated At'),
            'background' => Yii::t('catalog', 'Background image'),
            'status' => Yii::t('catalog', 'Status'),
        ];
    }


    /**
     * @inheritdoc
     */
    public function getFrontendViewLink()
    {
        return ['/catalog/category/view', 'slug' => $this->slug];
    }

    /**
     * @inheritdoc
     */
    public static function frontendViewLink($model)
    {
        return ['/catalog/category/view', 'slug' => $model['slug']];
    }

    /**
     * @inheritdoc
     */
    public function getBackendViewLink()
    {
        return ['/admin/catalog/category/update', 'id' => $this->id];
    }

    /**
     * @inheritdoc
     */
    public static function backendViewLink($model)
    {
        return ['/admin/catalog/category/update', 'id' => $model['id']];
    }

}
