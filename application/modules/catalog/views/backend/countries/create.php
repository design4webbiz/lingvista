<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCountries */

$this->title = Yii::t('catalog', 'Create Catalog Countries');
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-countries-create">

    <?= $this->render('_form', [
        'model' => $model,
        'lang'  => $lang,
    ]) ?>

</div>
