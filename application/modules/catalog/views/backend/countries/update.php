<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCountries */

$this->title = Yii::t('catalog', 'Update {modelClass}: ', [
    'modelClass' => 'Catalog Countries',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('catalog', 'Update').': '.$model->id;
?>
<div class="catalog-countries-update">

    <?= $this->render('_form', [
        'model' => $model,
        'lang'  => $lang,
    ]) ?>

</div>
