<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCategory */

$this->title = Yii::t('catalog', 'Update {modelClass}: ', [
    'modelClass' => 'Catalog Category',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('catalog', 'Update').': '.$model->id;
?>
<div class="catalog-category-update">

    <?= $this->render('_form', [
        'model' => $model,
        'lang'  => $lang,
    ]) ?>

</div>
