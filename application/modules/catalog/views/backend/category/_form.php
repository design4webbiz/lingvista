<?php

use yii\helpers\Html;
use app\modules\admin\widgets\Button;
use app\modules\admin\widgets\ActiveForm;
use app\modules\language\models\Language;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCategory */
/* @var $form yii\widgets\ActiveForm */
/* @var $lang \app\modules\language\models\Language */

$asset = \app\templates\backend\base\assets\BaseAsset::register($this);

// Language
if($model->isNewRecord) {
    $langueBtn = [
        'label' => '<img src="'.$asset->baseUrl.'/images/flags/'.$lang->url.'.png" alt="'.$lang->url.'"/> '.Yii::t('admin', 'Language'),
        'options' => [
            'form' => 'portfolio-id',
            'type' => 'submit',
        ],
        'encodeLabel' => false,
        'icon' => false,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-default'
    ];
} else {
    $allLang = Language::getLanguages();
    $langueBtnItems = [];
    foreach($allLang as $item) {
        if($lang->language_id != $item->language_id) {
            $langueBtnItems[] = [
                'label' => '<img src="'.$asset->baseUrl.'/images/flags/'.$item->url.'.png" alt="'.$item->url.'"/> '.$item->name,
                'url' => ['update', 'id' => $model->id, 'language' => $item->url]
            ];
        }
    }


    $langueBtn = [
        'label' => '<img src="'.$asset->baseUrl.'/images/flags/'.$lang->url.'.png" alt="'.$lang->url.'"/> '.$lang->name,
        'options' => [
            'class' => 'btn blue btn-outline btn-circle btn-sm',
            'data-hover'=>"dropdown",
            'data-close-others'=>"true",
        ],
        'encodeLabel' => false,
        'dropdown' => [
            'encodeLabels' => false,
            'options' => ['class' => 'pull-right'],
            'items' => $langueBtnItems,
        ],
    ];
}

$this->params['actions_buttons'] = [
    $langueBtn,
    [
        'label' => $model->isNewRecord ? Yii::t('catalog', 'Create') : Yii::t('catalog', 'Update'),
        'options' => [
            'form' => 'catalog-category-id',
            'type' => 'submit'
        ],
        'icon' => 'fa fa-save',
        'iconPosition' => Button::ICON_POSITION_LEFT,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-success'
    ],
    [
        'label' => Yii::t('catalog', 'Save & Continue Edit'),
        'options' => [
            'onclick' => 'sendFormReload("#catalog-category-id")',
        ],
        'icon' => 'fa fa-check-circle',
        'iconPosition' => Button::ICON_POSITION_LEFT,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-success'
    ]
]
?>

<div class="catalog-category-form">

    <?php $form = ActiveForm::begin([
        'options'=>[
            'id'=>'catalog-category-id',
        ],
    ]); ?>
    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true])->translatable() ?>
    <?php echo $form->field($model, 'slug')
        ->hint(Yii::t('admin', 'If you\'ll leave this field empty, slug will be generated automatically'))
        ->textInput(['maxlength' => true])->translatable() ?>

    <?php echo $form->field($model, 'title_h1')->textInput(['maxlength' => true])->translatable() ?>
    <?php echo $form->field($model, 'content')->widget(\app\widgets\Editor::className())->translatable() ?>

    <?= $form->field($model, 'background')->widget(\app\modules\admin\widgets\InputFile::className()); ?>
    <?= $form->field($model, 'published_at')->widget(\oakcms\datetimepicker\DateTime::className()); ?>
    <?= $form->field($model, 'created_at')->staticField(date('d.m.Y H:i', $model->created_at)) ?>
    <?= $form->field($model, 'updated_at')->staticField(date('d.m.Y H:i', $model->updated_at)) ?>
    <?= $form->field($model, 'status')->widget(\oakcms\bootstrapswitch\Switcher::className()) ?>
    <?= $form->field($model, 'show_home')->widget(\oakcms\bootstrapswitch\Switcher::className()) ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function sendFormReload(elm) {
        $(elm).append($("<input type='hidden' name='submit-type' value='continue'>"));
        $(elm).submit();
        return false;
    }
</script>
