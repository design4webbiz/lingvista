<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCategory */

$this->title = Yii::t('catalog', 'Create Catalog Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-category-create">

    <?= $this->render('_form', [
        'model' => $model,
        'lang'  => $lang,
    ]) ?>

</div>
