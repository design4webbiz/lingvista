<?php

use yii\helpers\Html;
use app\modules\admin\widgets\Button;
use app\modules\admin\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCurrencies */
/* @var $form yii\widgets\ActiveForm */

$this->params['actions_buttons'] = [
    [
        'label' => $model->isNewRecord ? Yii::t('catalog', 'Create') : Yii::t('catalog', 'Update'),
        'options' => [
            'form' => 'catalog-currencies-id',
            'type' => 'submit'
        ],
        'icon' => 'fa fa-save',
        'iconPosition' => Button::ICON_POSITION_LEFT,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-success'
    ],
    [
        'label' => Yii::t('catalog', 'Save & Continue Edit'),
        'options' => [
            'onclick' => 'sendFormReload("#catalog-currencies-id")',
        ],
        'icon' => 'fa fa-check-circle',
        'iconPosition' => Button::ICON_POSITION_LEFT,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-success'
    ]
];
?>

<div class="catalog-currencies-form">

    <?php $form = ActiveForm::begin([
        'options'=>[
            'id'=>'catalog-currencies-id',
        ],
    ]); ?>

    <?= $form->field($model, 'iso_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'unicode')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'position')->dropDownList([ 'before' => 'Before', 'after' => 'After', ], ['prompt' => '']) ?>
    <?= $form->field($model, 'comments')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'timestamp')->textInput() ?>
    <?= $form->field($model, 'equivalent_currency')->widget(\app\modules\admin\widgets\AceEditor::className(), ['mode'  => 'yaml', 'readOnly' => 'false']); ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function sendFormReload(elm) {
        $(elm).append($("<input type='hidden' name='submit-type' value='continue'>"));
        $(elm).submit();
        return false;
    }
</script>
