<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCurrencies */

$this->title = Yii::t('catalog', 'Update {modelClass}: ', [
    'modelClass' => 'Catalog Currencies',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('catalog', 'Update').': '.$model->id;
?>
<div class="catalog-currencies-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
