<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\search\CatalogCurrenciesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalog-currencies-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'iso_code') ?>

    <?= $form->field($model, 'symbol') ?>

    <?= $form->field($model, 'flag') ?>

    <?= $form->field($model, 'unicode') ?>

    <?php // echo $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'comments') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('catalog', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('catalog', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
