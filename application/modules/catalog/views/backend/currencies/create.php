<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogCurrencies */

$this->title = Yii::t('catalog', 'Create Catalog Currencies');
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Currencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-currencies-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
