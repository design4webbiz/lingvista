<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogItems */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-items-view">

    <p>
        <?php echo Html::a(Yii::t('catalog', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('catalog', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('catalog', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'specialization_id',
            'gender',
            'age',
            'catalog_country_id',
            'published_at',
            'created_at',
            'updated_at',
            'status',
            'is_hot',
            'is_free',
            'is_video',
            'widgetkit',
        ],
    ]) ?>

</div>
