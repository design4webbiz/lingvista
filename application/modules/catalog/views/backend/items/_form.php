<?php

use yii\helpers\Html;
use app\modules\admin\widgets\Button;
use app\modules\admin\widgets\ActiveForm;
use app\modules\language\models\Language;
use dosamigos\grid\EditableColumn;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogItems */
/* @var $form yii\widgets\ActiveForm */
/* @var $lang \app\modules\language\models\Language */

$js = '$(document).ready(function () {
        $(document).on(\'click\', \'#add_price\', function () {
            $(this).button(\'loading\');
            $.pjax({
                type       : \'POST\',
                url        : \'\',
                container  : \'#pjax-prices\',
                data       : {
                        CatalogPrices : {
                            catalog_items_id: $(\'#catalogprices-catalog_items_id\').val(),
                            catalog_currency_id: $(\'#catalogprices-catalog_currency_id\').val(),
                            price_from: $(\'#catalogprices-price_from\').val(),
                            price_to: $(\'#catalogprices-price_to\').val(),
                            convert: $(\'#catalogprices-convert\').prop(\'checked\') ? 1 : 0
                        }
                    },
                push       : true,
                replace    : false,
                "scrollTo" : false
            });
        });
    });';

$this->registerJs($js);

$asset = \app\templates\backend\base\assets\BaseAsset::register($this);

// Language
if($model->isNewRecord) {
    $langueBtn = [
        'label' => '<img src="'.$asset->baseUrl.'/images/flags/'.$lang->url.'.png" alt="'.$lang->url.'"/> '.Yii::t('admin', 'Language'),
        'options' => [
            'form' => 'portfolio-id',
            'type' => 'submit',
        ],
        'encodeLabel' => false,
        'icon' => false,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-default'
    ];
} else {
    $allLang = Language::getLanguages();
    $langueBtnItems = [];
    foreach($allLang as $item) {
        if($lang->language_id != $item->language_id) {
            $langueBtnItems[] = [
                'label' => '<img src="'.$asset->baseUrl.'/images/flags/'.$item->url.'.png" alt="'.$item->url.'"/> '.$item->name,
                'url' => ['update', 'id' => $model->id, 'language' => $item->url]
            ];
        }
    }


    $langueBtn = [
        'label' => '<img src="'.$asset->baseUrl.'/images/flags/'.$lang->url.'.png" alt="'.$lang->url.'"/> '.$lang->name,
        'options' => [
            'class' => 'btn blue btn-outline btn-circle btn-sm',
            'data-hover'=>"dropdown",
            'data-close-others'=>"true",
        ],
        'encodeLabel' => false,
        'dropdown' => [
            'encodeLabels' => false,
            'options' => ['class' => 'pull-right'],
            'items' => $langueBtnItems,
        ],
    ];
}

$this->params['actions_buttons'] = [
    $langueBtn,
    [
        'label' => $model->isNewRecord ? Yii::t('catalog', 'Create') : Yii::t('catalog', 'Update'),
        'options' => [
            'form' => 'catalog-items-id',
            'type' => 'submit'
        ],
        'icon' => 'fa fa-save',
        'iconPosition' => Button::ICON_POSITION_LEFT,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-success'
    ],
    [
        'label' => Yii::t('catalog', 'Save & Continue Edit'),
        'options' => [
            'onclick' => 'sendFormReload("#catalog-items-id")',
        ],
        'icon' => 'fa fa-check-circle',
        'iconPosition' => Button::ICON_POSITION_LEFT,
        'size' => Button::SIZE_SMALL,
        'disabled' => false,
        'block' => false,
        'type' => Button::TYPE_CIRCLE,
        'color' => 'btn-success'
    ]
]
?>

<div class="catalog-items-form">
    <?php $form = ActiveForm::begin([
        'options'=>[
            'enctype' => 'multipart/form-data',
            'id'=>'catalog-items-id',
        ],
    ]); ?>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1" data-toggle="tab" aria-expanded="true">
                    <i class="fa fa-file-text-o"></i> <?= Yii::t('catalog', 'Content') ?>
                </a>
            </li>
            <li class="">
                <a href="#tab_2" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-picture-o" aria-hidden="true"></i> <?= Yii::t('catalog', 'Images') ?>
                </a>
            </li>
            <li class="">
                <a href="#tab_3" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-newspaper-o"></i> <?= Yii::t('catalog', 'Publication') ?>
                </a>
            </li>
            <li class="">
                <a href="#tab_4" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-money" aria-hidden="true"></i> <?= Yii::t('catalog', 'Prices') ?>
                </a>
            </li>
            <li class="">
                <a href="#tab_5" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-star"></i> <?= Yii::t('catalog', 'SEO') ?>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <?php echo $form->field($model, 'title')->textInput(['maxlength' => true])->translatable() ?>
                <?php echo $form->field($model, 'slug')
                    ->hint(Yii::t('admin', 'If you\'ll leave this field empty, slug will be generated automatically'))
                    ->textInput(['maxlength' => true])->translatable() ?>

                <?php echo $form->field($model, 'identifier')->textInput(['maxlength' => true]) ?>
                <?php echo $form->field($model, 'widgetkit_title')->textInput() ?>

                <?= $form->field($model, 'widgetkit')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                        \app\modules\widgets\models\Widgetkit::find()->all(),
                        'id',
                        'name'
                    )
                ) ?>

                <?= $form->field($model, 'review_block_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                        \app\modules\text\models\Text::find()->published()->andWhere(['layout' => 'video_reviews'])->all(),
                        'id',
                        'title'
                    )
                ) ?>

                <?= $form->field($model, 'specialization_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                        \app\modules\catalog\models\CatalogCategory::find()->published()->all(),
                        'id',
                        'title'
                    )
                ) ?>
                <?= $form->field($model, 'gender')->dropDownList($model->getGender()) ?>
                <?= $form->field($model, 'catalog_country_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                        \app\modules\catalog\models\CatalogCountries::find()->published()->all(),
                        'id',
                        'title'
                    )
                )?>

                <?= $form->field($model, 'age')->textInput()->translatable() ?>
                <?= $form->field($model, 'experience')->textInput()->translatable() ?>
                <?= $form->field($model, 'duties')->widget(\app\widgets\Editor::className())->translatable() ?>
                <?= $form->field($model, 'conditions_of_employment')->widget(\app\widgets\Editor::className())->translatable() ?>
                <?= $form->field($model, 'requirements')->widget(\app\widgets\Editor::className())->translatable() ?>
                <?= $form->field($model, 'work_schedule')->widget(\app\widgets\Editor::className())->translatable() ?>
                <?= $form->field($model, 'salary')->widget(\app\widgets\Editor::className())->translatable() ?>
            </div>
            <div class="tab-pane" id="tab_2">
                <?php if($model->isNewRecord): ?>
                    <div class="callout callout-info">
                        <p><?= Yii::t('catalog', 'Save form before adding images.') ?></p>
                    </div>
                <?php else: ?>
                    <?= \app\modules\gallery\widgets\Gallery::widget(['model' => $model]); ?>
                <?php endif; ?>
            </div>
            <div class="tab-pane" id="tab_3">
                <?= $form->field($model, 'published_at')->widget(\oakcms\datetimepicker\DateTime::className()); ?>
                <?= $form->field($model, 'created_at')->staticField(date('d.m.Y H:i', $model->created_at)) ?>
                <?= $form->field($model, 'updated_at')->staticField(date('d.m.Y H:i', $model->updated_at)) ?>
                <?= $form->field($model, 'status')->widget(\oakcms\bootstrapswitch\Switcher::className()) ?>
                <?= $form->field($model, 'is_hot')->widget(\oakcms\bootstrapswitch\Switcher::className()) ?>
                <?= $form->field($model, 'is_free')->widget(\oakcms\bootstrapswitch\Switcher::className()) ?>
                <?= $form->field($model, 'is_video')->widget(\oakcms\bootstrapswitch\Switcher::className()) ?>
                <?= $form->field($model, 'show_home')->widget(\oakcms\bootstrapswitch\Switcher::className()) ?>
            </div>
            <div class="tab-pane" id="tab_4">
                <?= $form->field($model, 'price_from')->textInput(); ?>
                <?= $form->field($model, 'price_to')->textInput() ?>
                <?= $form->field($model, 'currency_id')->dropDownList(\yii\helpers\ArrayHelper::map(
                    \app\modules\catalog\models\CatalogCurrencies::find()->all(),
                    'id',
                    'iso_code'
                )) ?>
<!--                --><?php //if($model->isNewRecord): ?>
<!--                    <div class="callout callout-info">-->
<!--                        <p>--><?//= Yii::t('catalog', 'Save form before adding prices.') ?><!--</p>-->
<!--                    </div>-->
<!--                --><?php //else: ?>
<!--                    --><?php //\yii\widgets\Pjax::begin(['id' => 'pjax-prices']); ?>
<!--                    <a href="#" class="btn btn-success" onclick="$('.product-add-price-form').toggle(); return false;">-->
<!--                        Добавить <span class="glyphicon glyphicon-plus add-price"></span>-->
<!--                    </a>-->
<!--                    <div class="product-add-price-form" style="--><?php //if(!$pricesModel->hasErrors()):?><?php //endif; ?><!--">-->
<!--                        --><?//= $form->errorSummary($pricesModel); ?>
<!--                        --><?//= Html::input('hidden', 'catalog_items_id', $model->id, ['class' => 'form-control', 'id' => 'catalogprices-catalog_items_id']) ?>
<!--                        <div class="form-group field-catalogprices-catalog_currency_id">-->
<!--                            <label class="col-md-3 control-label" for="catalogprices-catalog_currency_id">Currency</label>-->
<!--                            <div class="col-md-9">-->
<!--                                --><?//= Html::dropDownList('catalog_currency_id', '', \yii\helpers\ArrayHelper::map(
//                                    \app\modules\catalog\models\CatalogCurrencies::find()->all(),
//                                    'id',
//                                    'iso_code'
//                                ), ['class' => 'form-control', 'id' => 'catalogprices-catalog_currency_id']) ?>
<!--                                <div class="help-block"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group field-catalogprices-price_from">-->
<!--                            <label class="col-md-3 control-label" for="catalogprices-price_from">Price From</label>-->
<!--                            <div class="col-md-9">-->
<!--                                --><?//= Html::input('text', 'price_from', '', ['class' => 'form-control', 'id' => 'catalogprices-price_from']) ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group field-catalogprices-price_to">-->
<!--                            <label class="col-md-3 control-label" for="catalogprices-price_to">Price To</label>-->
<!--                            <div class="col-md-9">-->
<!--                                --><?//= Html::input('text', 'price_to', '', ['class' => 'form-control', 'id' => 'catalogprices-price_to']) ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group field-catalogprices-price_to">-->
<!--                            <label class="col-md-3 control-label" for="catalogprices-convert">Convert to other currencies</label>-->
<!--                            <div class="col-md-9">-->
<!--                                    --><?php
//                                    echo \oakcms\bootstrapswitch\Switcher::widget([
//                                            'id' => 'catalogprices-convert',
//                                            'name'    => 'convert',
//                                            'checked' => false
//                                        ]);
//                                    ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="">-->
<!--                            --><?//= Html::button('Добавить', ['class' => 'btn btn-primary', 'id' => 'add_price']) ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="mb-20"></div>-->
<!--                    --><?php
//                    echo \yii\grid\GridView::widget([
//                        'dataProvider' => $pricesDataProvider,
//                        'tableOptions' => ['class'=>'table table-striped table-bordered table-advance table-hover'],
//                        'columns'      => [
//                            [
//                                'attribute' => 'id',
//                                'filter' => false,
//                                'options' => ['style' => 'width: 25px;']
//                            ],
//                            [
//                                'attribute'       => 'catalog_currency_id',
//                                'value'           => function($model) {
//                                    return $model->currency->iso_code;
//                                },
//                                'filter' => false,
//                                'options'         => ['style' => 'width: 75px;'],
//                            ],
//                            [
//                                'class'           => EditableColumn::className(),
//                                'attribute'       => 'price_from',
//                                'url'             => ['price/edit-field'],
//                                'type'            => 'text',
//                                'editableOptions' => [
//                                    'mode' => 'inline',
//                                ],
//                                'options'         => ['style' => 'width: 40px;'],
//                            ],
//                            [
//                                'class'           => EditableColumn::className(),
//                                'attribute'       => 'price_to',
//                                'url'             => ['price/edit-field'],
//                                'type'            => 'text',
//                                'editableOptions' => [
//                                    'mode' => 'inline',
//                                ],
//                                'options'         => ['style' => 'width: 49px;'],
//                            ],
//                            ['class' => 'yii\grid\ActionColumn', 'controller' => 'price', 'template' => '{delete}', 'buttonOptions' => ['class' => 'btn btn-default'], 'options' => ['style' => 'width: 30px;']],
//                        ],
//                    ]);
//                    \yii\widgets\Pjax::end();
//                    ?>
<!---->
<!--                --><?php //endif; ?>
            </div>
            <div class="tab-pane" id="tab_5">
                <?= $form->field($model, 'meta_title')->textarea()->translatable() ?>
                <?= $form->field($model, 'meta_keywords')->textarea()->translatable() ?>
                <?= $form->field($model, 'meta_description')->textarea()->translatable() ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    function sendFormReload(elm) {
        $(elm).append($("<input type='hidden' name='submit-type' value='continue'>"));
        $(elm).submit();
        return false;
    }
</script>
