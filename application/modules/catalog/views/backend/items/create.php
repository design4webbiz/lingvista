<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogItems */

$this->title = Yii::t('catalog', 'Create Catalog Items');
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-items-create">

    <?= $this->render('_form', [
        'model' => $model,
        'lang'  => $lang,
    ]) ?>

</div>
