<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\CatalogItems */

$this->title = Yii::t('catalog', 'Update {modelClass}: ', [
    'modelClass' => 'Catalog Items',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('catalog', 'Catalog Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('catalog', 'Update').': '.$model->id;
?>
<div class="catalog-items-update">

    <?= $this->render('_form', [
        'model' => $model,
        'lang'  => $lang,
        'currencies'         => $currencies,
        'pricesModel'        => $pricesModel,
        'pricesSearchModel'  => $pricesSearchModel,
        'pricesDataProvider' => $pricesDataProvider,
    ]) ?>

</div>
