<?php
/**
 * @var $dataProviders \yii\data\ActiveDataProvider
 * @var $models \app\modules\catalog\models\CatalogCategory
 */

use yii\widgets\ListView;

/** @var \app\modules\menu\models\MenuItem $menu */
$menu = Yii::$app->menuManager->getActiveMenu();
if ($menu) {
    $this->title = $menu->isProperContext() ? $menu->title : Yii::t('content', 'News');
    $this->params['breadcrumbs'] = $menu->getBreadcrumbs(false);
} else {
    $this->title = Yii::t('content', 'All');
}

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => '_item',
    'layout' => "<div class=\"block_vakansii\"><div class='list_vakansii'>{items}</div></div>\n<div class='list_pagination'>{pager}</div>",
    'pager' => [
        //'registerLinkTags' => true,
        'options' => [
            'class' => 'inline-layout'
        ],
        'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
        'prevPageLabel' => '<i class="fa fa-angle-right"></i>'
    ]
]);
