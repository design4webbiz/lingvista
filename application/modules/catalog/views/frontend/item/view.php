<?php

use yii\helpers\Url;
use app\helpers\ArrayHelper;
use app\modules\text\api\Text;
use app\modules\catalog\models\CatalogItems;
use app\modules\catalog\models\CatalogItemsLang;
use app\modules\catalog\models\CatalogCurrencies;

/**
 * @var $model CatalogItems
 * @var $this  app\components\CoreView
 * @var $menu  app\modules\menu\models\MenuItem
 */

$menu = Yii::$app->menuManager->getActiveMenu();
if ($menu) {
    $this->title = $menu->isProperContext() ? $menu->title : $model->title;
    $this->params['breadcrumbs'] = $menu->getBreadcrumbs(true);
} else {
    $this->title = $model->title;
    $this->params['breadcrumbs'] = [
        ['label' => $model->category->title, 'url' => $model->category->frontendViewLink],
    ];
}

$this->pageTitle = $model->title;
$this->pageTitleHeading = 'h1';

$this->registerJsFile('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$this->registerJsFile('//yastatic.net/share2/share.js');

$otherJobs = CatalogItems::find()
    ->joinWith(['translations'])
    ->where([
        CatalogItems::tableName() . '.status'       => CatalogItems::STATUS_PUBLISHED,
        CatalogItemsLang::tableName() . '.language' => Yii::$app->language,
    ])
    ->andWhere(['<>', CatalogItems::tableName() . '.id', $model->id])
    ->limit(4)
    ->all();
?>
<div class="inline-layout block_info_vakansii">
    <div class="block_text_vakansii">
        <div class="block_info_vakansii inline-layout">
            <div class="block-img">
                <div class="image-holder">
                    <img src="<?= $model->getImage()->getUrl('245х150'); ?>" class="fake-img">
                    <div style="background: url('<?= $model->getImage()->getUrl('245х150'); ?>') no-repeat center;background-size: cover;" class="img"></div>
                </div>
            </div>
            <div class="info_vakansii">
                <?php if ($model->is_hot || $model->is_free): ?>
                    <div class="inline-layout col-2">
                        <div class="column">
                            <div class="status"><?= Yii::t('catalog', 'Status') //статус  ?></div>
                        </div>
                        <div class="column">
                            <?php if ($model->is_hot): ?>
                                <div class="status hot"><?= Yii::t('catalog', 'Hot') ?></div>
                            <?php elseif ($model->is_free): ?>
                                <div class="status free"><?= Yii::t('catalog', 'Free') ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($model->identifier != ""): ?>
                    <div class="inline-layout col-2">
                        <div class="column">
                            <div class="indificator"><?= Yii::t('catalog', 'Identifier') //Идентификатор  ?></div>
                        </div>
                        <div class="column">
                            <div class="indificator"><?= $model->identifier ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($model->gender != ""): ?>
                    <div class="inline-layout col-2">
                        <div class="column">
                            <div class="pol"><?= Yii::t('catalog', 'Floor') //пол  ?></div>
                        </div>
                        <div class="column">
                            <div class="pol"><?= $model->getGender($model->gender) ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($model->age != ""): ?>
                    <div class="inline-layout col-2">
                        <div class="column">
                            <div class="vozrast"><?= Yii::t('catalog', 'Age') //возраст  ?></div>
                        </div>
                        <div class="column">
                            <div class="vozrast"><?= $model->age ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($model->experience != ""): ?>
                    <div class="inline-layout col-2">
                        <div class="column">
                            <div class="oput"><?= Yii::t('catalog', 'Experience') //опыт  ?></div>
                        </div>
                        <div class="column">
                            <div class="oput"><?= $model->experience ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="inline-layout col-2">
                    <div class="column">
                        <div class="info_price"><?= Yii::t('catalog', 'Salary') //зарплата  ?></div>
                    </div>
                    <div class="column">
                        <div class="info_price">
                            <?php
                            $currencies = CatalogCurrencies::find()->asArray()->all();
                            foreach ($currencies as $currency): ?>
                                <?php if ($currency['id'] == $model->currency_id): ?>
                                    <p class="<?= strtolower($currency['iso_code']) ?>"><?= $currency['iso_code'] ?>
                                        = <?= $model->price_from . ' - ' . $model->price_to ?></p>
                                <?php else: ?>
                                    <?php
                                    $coreCurrency = ArrayHelper::search($currencies, $model->currency_id, 'id');
                                    $equivalents = \Symfony\Component\Yaml\Yaml::parse($coreCurrency['equivalent_currency']);
                                    $price_from = round($model->price_from * $equivalents[$currency['iso_code']]);
                                    $price_to = round($model->price_to * $equivalents[$currency['iso_code']]);
                                    ?>
                                    <p class="<?= strtolower($currency['iso_code']) ?>"><?= $currency['iso_code'] ?> ≈ <?= $price_from . ' - ' . $price_to ?></p>
                                <?php endif ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="inline-layout col-2">
                    <div class="column">
                        <div class="strana">
                            <?= Yii::t('catalog', 'Country') //страна  ?>
                        </div>
                    </div>
                    <div class="column">
                        <div style="background: url('<?= $model->country->flag ?>') no-repeat left center" class="strana"><?= $model->country->title ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_desc_vakansii">
            <h4><?= Yii::t('catalog', 'Vacancy description'); //описание вакансии  ?></h4>
            <div class="text_vakansii">
                <div class="item inline-layout">
                    <div class="name"><?= Yii::t('catalog', 'Duties'); //Обязанности  ?></div>
                    <div class="decs_vakansii">
                        <?= $model->duties ?>
                    </div>
                </div>
                <div class="item inline-layout">
                    <div class="name"><?= Yii::t('catalog', 'Conditions of employment') //Условия трудоустройства  ?></div>
                    <div class="decs_vakansii">
                        <?= $model->conditions_of_employment ?>
                    </div>
                </div>
                <div class="item inline-layout">
                    <div class="name"><?= Yii::t('catalog', 'Requirements') //Требования  ?></div>
                    <div class="decs_vakansii">
                        <?= $model->requirements ?>
                    </div>
                </div>
                <div class="item inline-layout">
                    <div class="name"><?= Yii::t('catalog', 'Work schedule') //Рабочий график  ?></div>
                    <div class="decs_vakansii">
                        <?= $model->work_schedule ?>
                    </div>
                </div>
                <div class="item inline-layout">
                    <div class="name"><?= Yii::t('catalog', 'Salary') //Рабочий график  ?></div>
                    <div class="decs_vakansii">
                        <?= $model->salary ?>
                    </div>
                </div>
                <a href="#form_rezume" class="button open_modal"><?= Yii::t('catalog', 'Add Resume'); // Добавить резюме  ?></a>
            </div>
        </div>
        <div data-services="facebook,vkontakte,twitter,odnoklassniki,gplus" class="ya-share2"></div>
        <div class="block_sertificat">
            <?php if ($model->widgetkit_title != ''): ?>
                <h3><?= $model->widgetkit_title ?></h3>
            <?php else: ?>
                <h3><?= Yii::t('catalog', 'Licenses for implementation of work on employment') ?></h3>
            <?php endif; ?>
            [widgetkit id="<?= $model->widgetkit ?>"]
        </div>
    </div>
    <div class="sidebar">
        <?php if (isset($model->review_block_id) && $model->review_block_id != ''): ?>
            <?= Text::get($model->review_block_id, true) ?>
        <?php endif; ?>
        <?= Text::get('sidebar_catalog_item') ?>
    </div>
</div>
<?php if (count($otherJobs) > 0): ?>
</div>
<div class="category_link_more">
    <h3><?= Yii::t('catalog', 'Other jobs in this category') ?></h3>
    <div class="list-last-news">
        <div class="container-fluid inline-layout col-2">
            <?php foreach ($otherJobs as $item): ?>
                <a href="<?= Url::to($item->frontendViewLink) ?>" class="item">
                    <div class="row-table">
                        <div class="td-row"><span><?= $item->title ?></span></div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div>
    <?php endif; ?>
