<?php
/**
 * @var $model \app\modules\catalog\models\CatalogItems
 */
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="item">
    <div class="block_info_vakansii inline-layout">
        <div class="block-img">
            <a href="<?= Url::to(['/catalog/item/view', 'catslug' => $model->category->slug, 'slug' => $model->slug]) ?>">
                <div class="image-holder">
                    <img src="<?= $model->getImage()->getUrl('245х150'); ?>" alt="<?= $model->getImage()->alt; ?>" class="fake-img">
                    <div style="background: url('<?= $model->getImage()->getUrl('245х150'); ?>') no-repeat center;background-size: cover;" class="img"></div>
                </div>
            </a>
        </div>
        <div class="info_vakansii-price">
            <h4><?= Html::a($model->title, ['/catalog/item/view', 'catslug' => $model->category->slug, 'slug' => $model->slug]) ?></h4>
            <div class="info_vakansii">
                <div class="row inline-layout col-2">
                    <div class="column">
                        <?php if($model->is_hot || $model->is_free):?>
                            <div class="status"><?= Yii::t('catalog', 'Status') //статус ?></div>
                        <?php endif; ?>
                        <?php if($model->identifier != ""):?>
                            <div class="indificator"><?= Yii::t('catalog', 'Identifier') //Идентификатор ?></div>
                        <?php endif; ?>
                        <?php if($model->gender != ""):?>
                            <div class="pol"><?= Yii::t('catalog', 'Floor') //пол ?></div>
                        <?php endif; ?>
                        <?php if($model->age != ""):?>
                            <div class="vozrast"><?= Yii::t('catalog', 'Age') //возраст ?></div>
                        <?php endif; ?>
                        <?php if($model->experience != ""):?>
                            <div class="oput"><?= Yii::t('catalog', 'Experience') //опыт ?></div>
                        <?php endif; ?>
                        <div class="strana"><?= Yii::t('catalog', 'Country') //страна ?></div>

                    </div>
                    <div class="column">
                        <?php if($model->is_hot): ?>
                            <div class="status hot"><?= Yii::t('catalog', 'Hot') ?></div>
                        <?php elseif($model->is_free): ?>
                            <div class="status free"><?= Yii::t('catalog', 'Free') ?></div>
                        <?php endif; ?>
                        <?php if($model->identifier != ""):?>
                            <div class="indificator"><?= $model->identifier ?></div>
                        <?php endif; ?>
                        <?php if($model->gender != ""):?>
                            <div class="pol"><?= $model->getGender($model->gender) ?></div>
                        <?php endif; ?>
                        <?php if($model->age != ""):?>
                            <div class="vozrast"><?= $model->age ?></div>
                        <?php endif; ?>
                        <?php if($model->experience != ""):?>
                            <div class="oput"><?= $model->experience ?></div>
                        <?php endif; ?>
                        <div style="background: url('<?= $model->country->flag ?>') no-repeat left center" class="strana"><?= $model->country->title ?></div>
                    </div>
                </div>
            </div>
            <div class="info_price">
                <h4>зарплата</h4>
                <?php
                $currencies = \app\modules\catalog\models\CatalogCurrencies::find()->asArray()->all();
                foreach ($currencies as $currency): ?>
                    <?php if ($currency['id'] == $model->currency_id): ?>
                        <p class="<?= strtolower($currency['iso_code']) ?>"><?= $currency['iso_code'] ?>
                            = <?= $model->price_from . ' - ' . $model->price_to ?></p>
                    <?php else: ?>
                        <?php
                        $coreCurrency = \app\helpers\ArrayHelper::search($currencies, $model->currency_id, 'id');
                        $equivalents = \Symfony\Component\Yaml\Yaml::parse($coreCurrency['equivalent_currency']);
                        $price_from = round($model->price_from * $equivalents[$currency['iso_code']]);
                        $price_to = round($model->price_to * $equivalents[$currency['iso_code']]);
                        ?>
                        <p class="<?= strtolower($currency['iso_code']) ?>"><?= $currency['iso_code'] ?> ≈ <?= $price_from . ' - ' . $price_to ?></p>
                    <?php endif ?>
                <?php endforeach; ?>
                <a href="<?= Url::to(['/catalog/item/view', 'catslug' => $model->category->slug, 'slug' => $model->slug]) ?>" class="link_more">Посмотреть детальнее &gt;</a>
            </div>
        </div>
    </div>
</div>
