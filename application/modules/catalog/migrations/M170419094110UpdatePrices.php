<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogItems;
use yii\db\Migration;

class M170419094110UpdatePrices extends Migration
{
    public function up()
    {
        $this->addColumn(CatalogItems::tableName(), 'price_from', $this->integer(11));
        $this->addColumn(CatalogItems::tableName(), 'price_to', $this->integer(11));
        $this->addColumn(CatalogItems::tableName(), 'currency_id', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn(CatalogItems::tableName(), 'price_from');
        $this->dropColumn(CatalogItems::tableName(), 'price_to');
        $this->dropColumn(CatalogItems::tableName(), 'currency_id');
    }
}
