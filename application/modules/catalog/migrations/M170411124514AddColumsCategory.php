<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogCategoryLang;
use yii\db\Migration;

class M170411124514AddColumsCategory extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn(CatalogCategoryLang::tableName(), 'title_h1', $this->string(255));
        $this->addColumn(CatalogCategoryLang::tableName(), 'content', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn(CatalogCategoryLang::tableName(), 'title_h1');
        $this->dropColumn(CatalogCategoryLang::tableName(), 'content');
    }
}
