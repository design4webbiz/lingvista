<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogItems;
use app\modules\catalog\models\CatalogItemsLang;
use yii\db\Migration;

class M170419063006MoveCOlunm extends Migration
{
    public function safeUp()
    {
        $this->dropColumn(CatalogItems::tableName(), 'widgetkit_title');
        $this->addColumn(CatalogItemsLang::tableName(), 'widgetkit_title', $this->string(255));
    }

    public function safeDown()
    {
        $this->addColumn(CatalogItems::tableName(), 'widgetkit_title', $this->string(255));
        $this->dropColumn(CatalogItemsLang::tableName(), 'widgetkit_title');
    }
}
