<?php

namespace app\modules\catalog\migrations;

use yii\db\Migration;

class M170402233327UpdateItems extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropColumn('{{%catalog_items}}', 'age');
        $this->addColumn('{{%catalog_items_lang}}', 'age', $this->string(50)->null()->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%catalog_items_lang}}', 'age');
        $this->addColumn('{{%catalog_items}}', 'age', $this->integer(11)->null());
    }
}
