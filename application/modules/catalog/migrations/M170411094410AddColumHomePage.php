<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogCategory;
use app\modules\catalog\models\CatalogItems;
use yii\db\Migration;

class M170411094410AddColumHomePage extends Migration
{
    public function safeUp()
    {
        $this->addColumn(CatalogItems::tableName(), 'show_home', $this->boolean());
        $this->addColumn(CatalogCategory::tableName(), 'show_home', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn(CatalogItems::tableName(), 'show_home');
        $this->dropColumn(CatalogCategory::tableName(), 'show_home');
    }
}
