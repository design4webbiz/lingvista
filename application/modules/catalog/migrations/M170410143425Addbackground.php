<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogCategory;
use yii\db\Migration;

class M170410143425Addbackground extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn(CatalogCategory::tableName(), 'background', $this->string(255)->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn(CatalogCategory::tableName(), 'background');
    }
}
