<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogItems;
use yii\db\Migration;

class M170411163907AddColumsReviews extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn(CatalogItems::tableName(), 'review_block_id', $this->integer(11));
    }

    public function safeDown()
    {
        $this->dropColumn(CatalogItems::tableName(), 'review_block_id');
    }
}
