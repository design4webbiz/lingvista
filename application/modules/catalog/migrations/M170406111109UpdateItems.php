<?php

namespace app\modules\catalog\migrations;

use yii\db\Migration;

class M170406111109UpdateItems extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%catalog_items_lang}}', 'experience', $this->string(255)->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%catalog_items_lang}}', 'experience');
    }
}
