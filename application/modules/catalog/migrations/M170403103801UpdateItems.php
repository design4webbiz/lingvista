<?php

namespace app\modules\catalog\migrations;

use yii\db\Migration;

class M170403103801UpdateItems extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameColumn('{{%catalog_items_lang}}', 'catalog_category_id', 'catalog_items_id');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%catalog_items_lang}}', 'catalog_items_id', 'catalog_category_id');
    }

}
