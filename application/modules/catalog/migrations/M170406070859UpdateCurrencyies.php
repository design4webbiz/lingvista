<?php

namespace app\modules\catalog\migrations;

use yii\db\Migration;

class M170406070859UpdateCurrencyies extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%catalog_currencies}}', 'equivalent_currency', $this->string()->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%catalog_currencies}}', 'equivalent_currency');
    }
}
