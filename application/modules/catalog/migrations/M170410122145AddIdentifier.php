<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogItems;
use yii\db\Migration;

class M170410122145Addidentifier extends Migration
{
    public function safeUp()
    {
        $this->addColumn(CatalogItems::tableName(), 'identifier', $this->integer(11));
    }

    public function safeDown()
    {
        $this->dropColumn(CatalogItems::tableName(), 'identifier');
    }
}
