<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogItems;
use yii\db\Migration;

class M170418162030AddCertificatesTitle extends Migration
{
    public function safeUp()
    {
        $this->addColumn(CatalogItems::tableName(), 'widgetkit_title', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn(CatalogItems::tableName(), 'widgetkit_title');
    }
}
