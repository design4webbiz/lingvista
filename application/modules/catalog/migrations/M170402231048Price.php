<?php

namespace app\modules\catalog\migrations;

use yii\db\Migration;

class M170402231048Price extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        } else {
            $tableOptions = 'ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%catalog_prices}}',
            [
                'id'                  => $this->primaryKey(),
                'catalog_items_id'    => $this->integer(11),
                'catalog_currency_id' => $this->integer(11),
                'price_from'          => $this->integer(11),
                'price_to'            => $this->integer(11),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%catalog_currencies}}');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
