<?php

namespace app\modules\catalog\migrations;

use app\modules\catalog\models\CatalogItemsLang;
use yii\db\Migration;

class M170424111115AddSalary extends Migration
{
    public function up()
    {
        $this->addColumn(CatalogItemsLang::tableName(), 'salary', $this->text());
    }

    public function down()
    {
        $this->dropColumn(CatalogItemsLang::tableName(), 'salary');
        return false;
    }
}
