<?php

namespace app\modules\catalog\migrations;

use yii\db\Migration;

class m170323_211254_catalog extends Migration
{
    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        } else {
            $tableOptions = 'ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%catalog_category}}',
            [
                'id'           => $this->primaryKey(11),
                'published_at' => $this->integer(11)->notNull(),
                'created_at'   => $this->integer(11)->notNull(),
                'updated_at'   => $this->integer(11)->notNull(),
                'status'       => $this->integer(11)->notNull(),
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%catalog_category_lang}}',
            [
                'id'                  => $this->primaryKey(),
                'catalog_category_id' => $this->integer(11),

                'title' => $this->string(255)->notNull(),
                'slug'  => $this->string(255)->notNull(),

                'meta_title'       => $this->string(255)->null()->defaultValue(''),
                'meta_keywords'    => $this->string(255)->null()->defaultValue(''),
                'meta_description' => $this->string(255)->null()->defaultValue(''),

                'language' => $this->string(10)->notNull(),
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%catalog_items}}',
            [
                'id'                 => $this->primaryKey(),
                'specialization_id'  => $this->integer(11)->notNull()->comment('specialization'),
                'gender'             => $this->integer(11)->defaultValue(1)->comment('Gender'),
                'age'                => $this->integer(11)->defaultValue(1)->comment('Age'),
                'catalog_country_id' => $this->integer(11)->notNull()->comment('Country'),
                'published_at'       => $this->integer(11)->notNull(),
                'created_at'         => $this->integer(11)->notNull(),
                'updated_at'         => $this->integer(11)->notNull(),
                'status'             => $this->integer(11)->notNull(),
                'is_hot'             => $this->boolean()->defaultValue(0),
                'is_free'            => $this->boolean()->defaultValue(0),
                'is_video'           => $this->boolean()->defaultValue(0),
                'widgetkit'          => $this->integer()->comment('Widgetkit'),
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%catalog_items_lang}}',
            [
                'id'                  => $this->primaryKey(),
                'catalog_category_id' => $this->integer(11),

                'title' => $this->string(255)->notNull(),
                'slug'  => $this->string(255)->notNull(),

                'duties'                    => $this->string(),
                'conditions_of_employment'  => $this->string()->comment('Conditions of Employment'),
                'requirements'              => $this->string()->comment('Requirements'),
                'work_schedule'             => $this->string()->comment('Work schedule'),


                'meta_title'       => $this->string(255)->null()->defaultValue(''),
                'meta_keywords'    => $this->string(255)->null()->defaultValue(''),
                'meta_description' => $this->string(255)->null()->defaultValue(''),

                'language' => $this->string(10)->notNull(),
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%catalog_countries}}',
            [
                'id'                => $this->primaryKey(),
                'flag'              => $this->string(255)->notNull(),
                'published_at'      => $this->integer(11)->notNull(),
                'created_at'        => $this->integer(11)->notNull(),
                'updated_at'        => $this->integer(11)->notNull(),
                'status'            => $this->integer(11)->notNull()
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%catalog_countries_lang}}',
            [
                'id'                 => $this->primaryKey(),
                'catalog_country_id' => $this->integer(11),
                'title'              => $this->string(255)->notNull(),
                'language'           => $this->string(10)->notNull()
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%catalog_currencies}}',
            [
                'id'                 => $this->primaryKey(),
                'iso_code'           => $this->string(3),
                'symbol'             => $this->string(3),
                'flag'               => $this->string(255)->defaultValue(''),
                'unicode'            => $this->string(8),
                'position'           => "ENUM('before', 'after')",
                'comments'           => $this->string(255)->defaultValue(''),
                'timestamp'          => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%catalog_category}}');
        $this->dropTable('{{%catalog_category_lang}}');
        $this->dropTable('{{%catalog_items}}');
        $this->dropTable('{{%catalog_items_lang}}');
    }
}
