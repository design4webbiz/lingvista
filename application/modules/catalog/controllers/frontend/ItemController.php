<?php


namespace app\modules\catalog\controllers\frontend;


use app\components\Controller;
use app\modules\catalog\models\CatalogCategory;
use app\modules\catalog\models\CatalogItems;
use yii\web\NotFoundHttpException;

class ItemController extends Controller
{
    public function actionView($catslug, $slug) {
        $categoryModel = CatalogCategory::find()
            ->published()
            ->joinWith(['translations'])
            ->andWhere(['{{%catalog_category_lang}}.slug' => $catslug])
            ->one();

        $model = CatalogItems::find()
            ->published()
            ->joinWith(['translations'])
            ->andWhere(['{{%catalog_items_lang}}.slug' => $slug])
            ->one();

        if($model === null || $categoryModel === null) {
            throw new NotFoundHttpException(\Yii::t('system', 'The requested page does not exist.'));
        }

        return $this->render('view', [
            'model' => $model,
            'categoryModel' => $categoryModel
        ]);
    }
}
