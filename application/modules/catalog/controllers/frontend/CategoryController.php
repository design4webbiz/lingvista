<?php

namespace app\modules\catalog\controllers\frontend;


use app\components\Controller;
use app\modules\catalog\models\CatalogCategory;
use app\modules\catalog\models\CatalogItems;
use app\modules\catalog\models\CatalogItemsLang;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class CategoryController extends Controller
{

    public function actionAll()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CatalogItems::find()
                ->joinWith(['translations'])
                ->andWhere([
                    CatalogItemsLang::tableName() . '.language' => \Yii::$app->language,
                    CatalogItems::tableName() . '.status'       => CatalogItems::STATUS_PUBLISHED,
                ])
                ->orderBy([
                    CatalogItems::tableName() . '.published_at'      => SORT_DESC,
                    CatalogItems::tableName() . '.specialization_id' => SORT_ASC,
                ]),

            'pagination' => [
                'defaultPageSize' => 40,
                'forcePageParam'  => false,
                'pageSizeParam'   => false,
            ],
        ]);

        return $this->render('all', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($slug)
    {
        $model = CatalogCategory::find()
            ->joinWith(['translations'])
            ->where(['{{%catalog_category_lang}}.slug' => $slug])
            ->one();

        if ($model === null) {
            throw new NotFoundHttpException(\Yii::t('system', 'The requested page does not exist.'));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => CatalogItems::find()
                ->joinWith(['translations'])
                ->andWhere([
                    '{{%catalog_items}}.specialization_id' => $model->id,
                    '{{%catalog_items_lang}}.language'     => \Yii::$app->language,
                ])
                ->orderBy(['published_at' => SORT_DESC])
                ->published(),

            'pagination' => [
                'defaultPageSize' => 15,
                'forcePageParam'  => false,
                'pageSizeParam'   => false,
            ],
        ]);

        return $this->render('view', [
            'model'        => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
}
