<?php


namespace app\modules\catalog\controllers\frontend;


use app\components\Controller;
use app\modules\catalog\models\CatalogCategory;
use app\modules\catalog\models\forms\Filter;

class FilterController extends Controller
{
    public function actionIndex() {
        $searchModel = new Filter();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        $request = \Yii::$app->request->queryParams;
        if(
            empty($request['Filter']['title']) &&
            empty($request['Filter']['country']) &&
            empty($request['Filter']['price_from']) &&
            empty($request['Filter']['currency']) &&
            !empty($request['Filter']['specialization'])
        ) {
            $category = CatalogCategory::findOne($request['Filter']['specialization']);
            $this->redirect($category->getFrontendViewLink());
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
