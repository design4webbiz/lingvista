<?php

namespace app\modules\catalog\controllers\backend;

use app\modules\admin\components\behaviors\StatusController;
use app\modules\catalog\models\CatalogCurrencies;
use app\modules\catalog\models\CatalogPrices;
use app\modules\catalog\models\search\CatalogPricesSearch;
use Yii;
use app\modules\catalog\models\CatalogItems;
use app\modules\catalog\models\search\CatalogItemsSearch;
use app\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemsController implements the CRUD actions for CatalogItems model.
 */
class ItemsController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            [
                'class' => StatusController::className(),
                'model' => CatalogItems::className()
            ]
        ];
    }

    /**
     * Lists all CatalogItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CatalogItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CatalogItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $lang = $this->getDefaultLanguage();
        $model = new CatalogItems();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(Yii::$app->request->post('submit-type') == 'continue')
                return $this->redirect(['update', 'id' => $model->id]);
            else
                return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'lang'  => $lang,
            ]);
        }
    }

    /**
     * Updates an existing CatalogItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $language = false)
    {
        $lang = $this->getDefaultLanguage($language);
        $model = $this->findModel($id);
        $model->language = $lang->language_id;

        $currencies = CatalogCurrencies::find()->all();
        $pricesSearchModel = new CatalogPricesSearch();
        $pricesModel = new CatalogPrices();
        $typeParams = Yii::$app->request->queryParams;
        $typeParams['CatalogPricesSearch']['catalog_items_id'] = $model->id;
        $pricesDataProvider = $pricesSearchModel->search($typeParams);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(Yii::$app->request->post('submit-type') == 'continue')
                return $this->redirect(['update', 'id' => $model->id, 'language' => $lang->url]);
            else
                return $this->redirect(['index']);
        } else {
            if(Yii::$app->request->post()) {
                if($pricesModel->load(Yii::$app->request->post()) && $pricesModel->save() && $pricesModel->convert) {
                    $currency = $pricesModel->currency;
                    $equivalents = \Symfony\Component\Yaml\Yaml::parse($currency->equivalent_currency);
                    foreach ($equivalents as $iso_code => $equivalent) {
                        if(($currency = CatalogCurrencies::find()->select(['id'])->asArray()->where(['iso_code' => $iso_code])->one()) !== null) {
                            $equivalentPriceModel = new CatalogPrices();
                            $equivalentPriceModel->catalog_items_id    = $pricesModel->catalog_items_id;
                            $equivalentPriceModel->catalog_currency_id = $currency['id'];
                            $equivalentPriceModel->price_to            = round($pricesModel->price_to * $equivalent);
                            $equivalentPriceModel->price_from          = round($pricesModel->price_from * $equivalent);
                            $equivalentPriceModel->save();
                        }
                    }
                }
            }

            return $this->render('update', [
                'model'              => $model,
                'lang'               => $lang,
                'currencies'         => $currencies,
                'pricesModel'        => $pricesModel,
                'pricesSearchModel'  => $pricesSearchModel,
                'pricesDataProvider' => $pricesDataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing CatalogItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes items an existing SeoItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionDeleteIds()
    {
        $ids = Yii::$app->request->get('id');
        $id_arr = explode(',', $ids);
        foreach ($id_arr as $id) {
            $this->findModel($id)->delete();
        }
        return $this->back();
    }

    public function actionPublished()
    {
        $ids = Yii::$app->request->get('id');
        $id_arr = explode(',', $ids);
        foreach ($id_arr as $id) {
            if (($model = CatalogItems::findOne($id)) !== null) {
                $model->status = CatalogItems::STATUS_PUBLISHED;
                $model->save();
            }
        }
        return $this->back();
    }

    public function actionUnpublished()
    {
        $ids = Yii::$app->request->get('id');
        $id_arr = explode(',', $ids);
        foreach ($id_arr as $id) {
            if (($model = CatalogItems::findOne($id)) !== null) {
                $model->status = CatalogItems::STATUS_DRAFT;
                $model->save();
            }
        }
        return $this->back();
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, CatalogItems::STATUS_PUBLISHED);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, CatalogItems::STATUS_DRAFT);
    }

    /**
     * Finds the CatalogItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CatalogItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CatalogItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
