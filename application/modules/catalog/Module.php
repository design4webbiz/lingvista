<?php

namespace app\modules\catalog;

use app\components\events\FetchRoutersEvent;
use app\components\module\ModuleEventsInterface;
use app\modules\admin\widgets\events\MenuItemsEvent;
use app\modules\admin\widgets\Menu;
use app\modules\catalog\components\MenuRouterCatalog;
use app\modules\menu\behaviors\MenuUrlRule;
use app\modules\menu\widgets\events\MenuItemRoutesEvent;
use app\modules\menu\widgets\MenuItemRoutes;

class Module extends \app\components\module\Module implements ModuleEventsInterface
{
    public static $urlRulesBackend = [];

    /** @var array The rules to be used in Frontend Url management. */
    public static $urlRulesFrontend = [
        'catalog/filter'                               => 'catalog/filter/index',
        'catalog/view/all'                             => 'catalog/category/all',
        'catalog/<slug:[\w\-]+>'                       => 'catalog/category/view',
        'catalog/<catslug:[\w\-]+>/<slug:[\w\-]+>'     => 'catalog/item/view',
    ];

    /**
     * @param $event MenuItemsEvent
     */
    public function addAdminMenuItem($event)
    {
        $event->items['catalog'] = [
            'label' => \Yii::t('catalog', 'Catalog'),
            'icon'  => '<i class="fa fa-folder" aria-hidden="true"></i>',
            'items' => [
                [
                    'icon'  => '<i class="fa fa-file-text-o"></i>',
                    'label' => \Yii::t('content', 'Items'),
                    'url'   => ['/admin/catalog/items/index'],
                ],
                [
                    'icon'  => '<i class="fa fa-folder-o"></i>',
                    'label' => \Yii::t('content', 'Categories'),
                    'url'   => ['/admin/catalog/category/index'],
                ],
                [
                    'icon'  => '<i class="fa fa-money" aria-hidden="true"></i>',
                    'label' => \Yii::t('content', 'Currencies'),
                    'url'   => ['/admin/catalog/currencies/index'],
                ],
                [
                    'icon'  => '<i class="fa fa-globe" aria-hidden="true"></i>',
                    'label' => \Yii::t('content', 'Countries'),
                    'url'   => ['/admin/catalog/countries/index'],
                ],
            ],
        ];
    }

    /**
     * @param $event FetchRoutersEvent
     */
    public function addMenuRouter($event)
    {
        $event->routers['MenuRouterCatalog'] = MenuRouterCatalog::className();
    }

    /**
     * @param $event MenuItemRoutesEvent
     */
    public function addMenuItemRoutes($event)
    {
        $event->items['catalog'] = [
            'label' => \Yii::t('catalog', 'Catalog'),
            'items' => [
                [
                    'label' => \Yii::t('catalog', 'All Categories'),
                    'route' => 'catalog/category/all'
                ],
                [
                    'label' => \Yii::t('catalog', 'Category View'),
                    'url'   => [
                        '/admin/catalog/category/select',
                    ],
                ],
                [
                    'label' => \Yii::t('catalog', 'Item View'),
                    'url'   => [
                        '/admin/catalog/items/select',
                    ],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Menu::EVENT_FETCH_ITEMS => 'addAdminMenuItem',
            MenuUrlRule::EVENT_FETCH_MODULE_ROUTERS => 'addMenuRouter',
            MenuItemRoutes::EVENT_FETCH_ITEMS       => 'addMenuItemRoutes',
        ];
    }
}
