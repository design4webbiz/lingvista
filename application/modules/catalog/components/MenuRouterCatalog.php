<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-alpha.0.5
 */

namespace app\modules\catalog\components;

use app\modules\catalog\models\CatalogCategoryLang;
use app\modules\catalog\models\CatalogItemsLang;
use app\modules\menu\behaviors\MenuMap;
use app\modules\menu\behaviors\MenuRouter;
use app\modules\menu\behaviors\MenuRequestInfo;
use app\modules\menu\models\MenuItem;
use app\modules\catalog\models\CatalogCategory;
use app\modules\catalog\models\CatalogItems;

class MenuRouterCatalog extends MenuRouter
{
    private $_categoryPaths = [];
    private $_itemsPaths = [];

    /**
     * @inheritdoc
     */
    public function parseUrlRules()
    {
        return [
            [
                'menuRoute' => 'catalog/category/view',
                'handler'   => 'parseCategoryView',
            ],
            [
                'menuRoute' => 'catalog/category/all',
                'handler'   => 'parseCategoryAll',
            ],
            [
                'menuRoute' => 'catalog/item/view',
                'handler'   => 'parseItemView',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createUrlRules()
    {
        return [
            [
                'requestRoute'  => 'catalog/category/view',
                'requestParams' => ['slug'],
                'handler'       => 'createCategoryView',
            ],
            [
                'requestRoute'  => 'catalog/category/all',
                'requestParams' => [],
                'handler'       => 'createCategoryAll',
            ],
            [
                'requestRoute'  => 'catalog/item/view',
                'requestParams' => ['slug'],
                'handler'       => 'createItemView',
            ]
        ];
    }

    /**
     * @param MenuRequestInfo $requestInfo
     *
     * @return array
     */
    public function parseCategoryView($requestInfo)
    {
        if(isset($requestInfo->requestRoute) && $requestInfo->requestRoute != '') {
            return $this->findArticle($requestInfo);
        }

        /** @var CatalogCategory $menuCategory */
        if (
            $menuCategory = CatalogCategory::find()
                ->joinWith(['translations'])
                ->where([CatalogCategoryLang::tableName().'.slug' => $requestInfo->menuParams['slug']])
                ->published()
                ->one()
        ) {
            return ['catalog/category/view', ['slug' => $menuCategory->slug]];
        }
    }

    /**
     * @param MenuRequestInfo $requestInfo
     *
     * @return array
     */
    public function parseCategoryAll($requestInfo)
    {
        return ['catalog/category/all'];
    }

    /**
     * @param MenuRequestInfo $requestInfo
     *
     * @return array
     */
    public function parseItemView($requestInfo)
    {
        return $this->findArticle($requestInfo);
    }

    /**
     * @param MenuRequestInfo $requestInfo
     *
     * @return mixed|null|string
     */
    public function createCategoryView($requestInfo)
    {
        if ($path = $requestInfo->menuMap->getMenuPathByRoute(MenuItem::toRoute('catalog/category/view', ['slug' => $requestInfo->requestParams['slug']]))) {
            unset($requestInfo->requestParams['slug']);

            return MenuItem::toRoute($path, $requestInfo->requestParams);
        } else {
            return "catalog/" . $requestInfo->requestParams['slug'];
        }
    }

    /**
     * @param MenuRequestInfo $requestInfo
     *
     * @return mixed|null|string
     */
    public function createCategoryAll($requestInfo)
    {
        if ($path = $requestInfo->menuMap->getMenuPathByRoute(MenuItem::toRoute('catalog/category/all'))) {
            unset($requestInfo->requestParams['slug']);

            return MenuItem::toRoute($path, $requestInfo->requestParams);
        } else {
            return "catalog/view/all";
        }
    }

    /**
     * @param MenuRequestInfo $requestInfo
     *
     * @return mixed|null|string
     */
    public function createItemView($requestInfo)
    {
        if (
            $path = $requestInfo->menuMap->getMenuPathByRoute(
                MenuItem::toRoute(
                    'catalog/item/view',
                    [
                        'catslug' => $requestInfo->requestParams['catslug'],
                        'slug' => $requestInfo->requestParams['slug']
                    ]
                )
            )
        ) {
            unset($requestInfo->requestParams['slug']);

            return MenuItem::toRoute($path, $requestInfo->requestParams);
        }

        return $this->findItemMenuPath($requestInfo->requestParams['catslug'], $requestInfo->requestParams['slug'], $requestInfo->menuMap);
    }

    /**
     * Находит путь к пункту меню ссылающемуся на категорию $categoryId, либо ее предка
     * Если путь ведет к предку, то достраиваем путь категории $categoryId
     *
     * @param $categorySlug
     * @param $menuMap MenuMap
     *
     * @return null|string
     */
    private function findCategoryMenuPath($categorySlug, $menuMap)
    {
        /** @var Category $category */

        if (!isset($this->_categoryPaths[$menuMap->language][$categorySlug])) {
            if ($path = $menuMap->getMenuPathByRoute(MenuItem::toRoute('catalog/category/view', ['slug' => $categorySlug]))) {
                $this->_categoryPaths[$menuMap->language][$categorySlug] = $path;
            } else {
                $this->_categoryPaths[$menuMap->language][$categorySlug] = false;
            }
        }
        return $this->_categoryPaths[$menuMap->language][$categorySlug];
    }

    private function findItemMenuPath($catSlug, $itemSlug, $menuMap)
    {
        $category = CatalogCategory::find()
            ->select([CatalogCategory::tableName().'.id'])
            ->joinWith(['translations'])
            ->where([CatalogCategoryLang::tableName().'.slug' => $catSlug])
            ->published()
            ->asArray()
            ->one();

        $item = CatalogItems::find()
            ->select([CatalogItems::tableName().'.specialization_id', CatalogItems::tableName().'.id'])
            ->joinWith(['translations'])
            ->where([CatalogItemsLang::tableName().'.slug' => $itemSlug])
            ->published()
            ->asArray()
            ->one();

        if($category['id'] == $item['specialization_id']) {
            if (!isset($this->_itemsPaths[$menuMap->language][$itemSlug])) {
                if ($path = $menuMap->getMenuPathByRoute(MenuItem::toRoute('catalog/item/view', ['catslug' => $catSlug, 'slug' => $itemSlug]))) {
                    $this->_itemsPaths[$menuMap->language][$itemSlug] = $path;
                } elseif ($path = $this->findCategoryMenuPath($catSlug, $menuMap)) {
                    $this->_itemsPaths[$menuMap->language][$itemSlug] = $path . '/' . $itemSlug;
                } else {
                    $this->_itemsPaths[$menuMap->language][$itemSlug] = false;
                }
            }

            return $this->_itemsPaths[$menuMap->language][$itemSlug];
        }
    }

    protected function findArticle($requestInfo) {
        if(
            ($category = CatalogCategory::find()
                ->select([CatalogCategory::tableName().'.id'])
                ->joinWith(['translations'])
                ->where([CatalogCategoryLang::tableName().'.slug' => $requestInfo->menuParams['slug']])
                ->published()
                ->asArray()
                ->one()) &&
            ($item = CatalogItems::find()
                ->select([CatalogItems::tableName().'.specialization_id', CatalogItems::tableName().'.id'])
                ->joinWith(['translations'])
                ->where([CatalogItemsLang::tableName().'.slug' => $requestInfo->requestRoute])
                ->published()
                ->asArray()
                ->one()) &&
            $category['id'] == $item['specialization_id']
        ) {
            return ['catalog/item/view', ['catslug' => $requestInfo->menuParams['slug'], 'slug' => $requestInfo->requestRoute]];
        }
        return null;
    }
}
