<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'filter',
    'title' => Yii::t('text', 'Filter'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/filter/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/filter/view.php',
    'settings' => [

    ],
];
