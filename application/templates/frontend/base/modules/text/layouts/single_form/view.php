<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */
?>
<div class="<?= $model->getSetting('cssClass') ?>">
    [form_builder id="<?= $model->getSetting('form') ?>"]
</div>
