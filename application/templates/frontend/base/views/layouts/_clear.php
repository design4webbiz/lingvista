<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 06.04.2016
 * Project: oakcms
 * File name: _clear.php
 */

/* @var $this \app\components\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;

$bundle = \app\templates\frontend\base\assets\BaseAsset::register($this);


$isHome = (Url::to() == Url::home()) ? true : false;
if ($isHome) {
    $this->bodyClass[] = 'home';
}

$this->bodyClass[] = 'mod_' . Yii::$app->controller->module->id;
$this->bodyClass[] = Yii::$app->controller->id . '_' . Yii::$app->controller->action->id;
$this->bodyClass[] = 'lang_' . strtolower(Yii::$app->language);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!-- Created by Volodumur Grivinskiy Design4web.biz -->
<html lang="<?= Yii::$app->language ?>">
<head>
    <base href="<?= Yii::$app->homeUrl ?>">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="images/icons/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="images/icons/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/icons/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="images/icons/favicons/manifest.json">
    <link rel="mask-icon" href="images/icons/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <?php
    echo Html::csrfMetaTags();
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()], 'canonical');
    $this->renderMetaTags();
    $this->head();
    ?>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="<?= implode(' ', (array)$this->bodyClass) ?>">
<?php $this->beginBody() ?>
<?= $content ?>
<?= \app\modules\text\api\Text::get('modal_form'); ?>
<div id="overlay"></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
